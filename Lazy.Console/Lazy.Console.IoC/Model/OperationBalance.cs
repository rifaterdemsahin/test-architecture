﻿using System;

namespace Lazy.Console.IoC.Model
{
    public class OperationBalance
    {
        private Lazy<User> _user;


        public OperationBalance(Lazy<User> user)
        {
            _user = user;

        }

        public OperationBalance()
        {
         
        }


        public Lazy<User> MainUser { get; set; }

        public int Amount { get; set; }
    }
}