﻿using System;
using System.Collections.Generic;

namespace Lazy.Console.IoC.Model
{
    public class User
    {
        private readonly Lazy<OperationBalance> _operationBalance;

        public User(Lazy<OperationBalance> operationBalance)
        {
            _operationBalance = operationBalance;
       
        }

        public List<OperationBalance> AllOperations { get; set; }


        public OperationBalance LatestOperation { get { return _operationBalance.Value; }}
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}