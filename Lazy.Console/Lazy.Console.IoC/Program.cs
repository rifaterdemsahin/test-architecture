﻿using System.Collections.Generic;
using Autofac;
using Lazy.Console.IoC.Model;
using System;

namespace Lazy.Console.IoC
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Laxy loading with autofac");
            var container = ConfigureIoC();


            var myOperation = container.Resolve<Lazy<OperationBalance>>();

            System.Console.WriteLine("Operation and user is defined");
            myOperation.Value.MainUser.Value.Name = "Alican Demirer";
            myOperation.Value.MainUser.Value.LatestOperation.Amount = 33;
            System.Console.WriteLine("My name is " + myOperation.Value.MainUser.Value.Name);
            System.Console.WriteLine("My latest operation :  " + myOperation.Value.MainUser.Value.LatestOperation.Amount);

            myOperation.Value.MainUser.Value.AllOperations = new List<OperationBalance>();
            for (int i = 0; i < 1000; i++)
                myOperation.Value.MainUser.Value.AllOperations.Add(new OperationBalance { Amount = new Random().Next(1, 100), MainUser = myOperation.Value.MainUser });

            System.Console.WriteLine("Program success");
            System.Console.Read();
        }

        private static IContainer ConfigureIoC()
        {
            var builder = new ContainerBuilder();


            builder.RegisterType<Lazy<User>>().As<Lazy<User>>();
            builder.RegisterType<Lazy<OperationBalance>>().As<Lazy<OperationBalance>>();

            var container = builder.Build();
            return container;
        }
    }
}