﻿using System;

namespace Lazy.Console
{
    public class OperationBalance
    {
        private readonly Lazy<User> _iamLAzy = new Lazy<User>();

        public User MainUser
        {
            get { return _iamLAzy.Value; }

        }

    }
}