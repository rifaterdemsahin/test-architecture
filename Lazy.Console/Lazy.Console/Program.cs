﻿namespace Lazy.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var myObject = new OperationBalance();
            System.Console.WriteLine("MyClassHasLazyProperty is defined");
            myObject.MainUser.Name = "Lazy Man";
            System.Console.WriteLine("My name is"+ myObject.MainUser.Name);
            System.Console.Read();
        }
    }
}