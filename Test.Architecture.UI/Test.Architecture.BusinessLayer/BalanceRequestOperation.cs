﻿using Test.Architecture.Domain.Model;

namespace Test.Architecture.BusinessLayer
{
    public class BalanceRequestOperation
    {
        /// <summary>
        /// Needed for IoC
        /// </summary>
        private readonly BalanceOperation _balanceOperation;

        public BalanceRequestOperation(BalanceOperation balanceOperation)
        {
            _balanceOperation = balanceOperation;
        }



        /// <summary>
        /// Non-Complex Operation for a THIN service layer
        /// Logic here makes it THICK...>>> losing objects states goes to anemic
        /// 
        /// Only Cross cutting logic should be HERE, non related domain actions
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public bool SaveWithdrawalMoney(int amount)
        {
            var result = false;
            _balanceOperation.Amount = amount;
            if (_balanceOperation.Validate())
                result =_balanceOperation.Save();
            return result;
        }
    }
}
