﻿using System;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using Autofac.Core;
using Autofac.Core.Lifetime;
using Autofac.Core.Registration;
using Autofac.Extras.CommonServiceLocator;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using Test.Architecture.Common.Services;
using Test.Architecture.Domain.Interface;
using Microsoft.Practices.ServiceLocation;

namespace Test.Architecture.Common.Aop
{
    /// <summary>
    /// http://codereview.stackexchange.com/questions/20341/inject-dependency-into-postsharp-aspect
    /// 
    /// Critical Service locator
    /// https://www.nuget.org/packages/Autofac.Extras.CommonServiceLocator/
    /// </summary>
    [Serializable]
    public class ExceptionAspect : OnExceptionAspect
    {
        public FlowBehavior Behavior { get; set; }

        private IExceptionConcern _exceptionConcern;

        public override void RuntimeInitialize(MethodBase method)
        {
            var MyLogger = GlobalServiceLocator.InstanceMemoryLogger(new LifetimeScope(new ComponentRegistry()));
            _exceptionConcern = new ExceptionConcern(MyLogger);

            //base.RuntimeInitialize(method);
        }

        public override void OnException(MethodExecutionArgs args)
        {
           if (Microsoft.Practices.ServiceLocation.ServiceLocator.IsLocationProviderSet)
           {
               var logResult = ServiceLocator.Current.GetInstance<ILogger>().Log("Logging in exception aspect!!!!!");
           }
            _exceptionConcern.OnException(new MethodExecutionArgsAdapter(args));

        }
    }
}
