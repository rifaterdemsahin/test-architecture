﻿using System;
using Autofac;
using Test.Architecture.Domain.Interface;

namespace Test.Architecture.Common.Aop
{
    public class ExceptionConcern : IExceptionConcern
    {
        private IComponentContext _context;
        public ExceptionConcern(IComponentContext context)
    {
        _context = context;
    }


        private readonly ILogger Logger;

        public ExceptionConcern(ILogger logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// 1 >>>>> Postsharp for AOP 
        /// 2 >>>>> Different loggers in different methods.
        /// </summary>
        public LogType LoggerType { get; set; }

        public String Message { get; set; }

        public Type ExceptionType { get; set; }

        public void OnException(IExceptionConcernArgs args)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            var msg = DateTime.Now + ": " + Message + Environment.NewLine;
            msg += string.Format("{0}: Error running {1}. {2}{3}{4}", DateTime.Now, args.ExceptionMetodName, args.ExceptionMessage, Environment.NewLine, args.ExceptionStackTrace);
            Console.WriteLine("***************Exception Aspect Start****************:");
            Console.WriteLine(msg);
            Console.WriteLine("***************Exception Aspect End****************:");
           // args.FlowBehavior = FlowBehavior.Continue.ToString();
            Console.ForegroundColor = ConsoleColor.Green;

            switch (LoggerType)
            {
                case LogType.Memory:
                    Logger.Log("Logging in MEMORY!!!!! exception aspect");
                    break;

                case LogType.Service:
                    Logger.Log("Logging in SERVICE exception aspect");
                    break;
            }
        }
        public Type GetExceptionType(System.Reflection.MethodBase targetMethod)
        {
            return ExceptionType;
        }


    }


}
