﻿using PostSharp.Aspects;

namespace Test.Architecture.Common.Aop
{
    public interface IExceptionConcern
    {
        void OnException(IExceptionConcernArgs args);
    }
}