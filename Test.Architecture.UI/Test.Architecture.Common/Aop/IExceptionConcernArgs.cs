﻿namespace Test.Architecture.Common.Aop
{
    public interface IExceptionConcernArgs
    {
        string FlowBehavior { get;  }
        string ExceptionMetodName { get; }
        string ExceptionMessage { get;  }
        string ExceptionStackTrace { get; }
    }
}