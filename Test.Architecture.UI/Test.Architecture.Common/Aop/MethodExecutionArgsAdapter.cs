﻿using PostSharp.Aspects;

namespace Test.Architecture.Common.Aop
{
    public class MethodExecutionArgsAdapter : IExceptionConcernArgs
    {
        private readonly MethodExecutionArgs _methodExecutionArgs;

        public MethodExecutionArgsAdapter(MethodExecutionArgs methodExecutionArgs)
        {
            _methodExecutionArgs = methodExecutionArgs;
        }

        public string FlowBehavior
        {
            get
            {
                return _methodExecutionArgs.FlowBehavior.ToString();
            }
        }
        public string ExceptionMetodName
        {
            get
            {
                return _methodExecutionArgs.Method.Name;
            }
        }
        public string ExceptionMessage
        {
            get
            {
                return _methodExecutionArgs.Exception.Message;
            }
        }
        public string ExceptionStackTrace
        {
            get
            {
                return _methodExecutionArgs.Exception.StackTrace;
            }
        }
    }
}