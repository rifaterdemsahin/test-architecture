﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Test.Architecture.Common.Services;
using Test.Architecture.Domain.Interface;

namespace Test.Architecture.Common
{
    public class GlobalServiceLocator
    {
        //https://code.google.com/p/autofac/wiki/ExistingApplications
        public static MemoryLogger InstanceMemoryLogger(IComponentContext arg)
        {
            //TODO: Singleton
            var result = new MemoryLogger();
            //Interface olmadığı için tak çıkart yok.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //result.Message(arg[0].ToString());

            //Container composite rootta kalıyor.!!!!
         //    ApplicationContainer.Instance.Resolve<ILogger>().GetNext();


            return result;
        }

        public static ServiceLogger InstanceServiceLogger(IComponentContext arg)
        {
            return new ServiceLogger();
        }

    }
}
