﻿namespace Test.Architecture.Common
{
    public interface ILoggerFactory<out T>
        where T : class
    {
        T CreateInstance();
    }
}