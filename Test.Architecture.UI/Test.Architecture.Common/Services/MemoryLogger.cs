﻿using System;
using Test.Architecture.Domain.Interface;

namespace Test.Architecture.Common.Services
{
    public class MemoryLogger : ILogger
    {
        /// <summary>
        /// Important injected into the Common
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool Log(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Called the MEMORY for fast logging message:" + message);
            Console.ForegroundColor = ConsoleColor.Green;

            return true;
        }
    }
}