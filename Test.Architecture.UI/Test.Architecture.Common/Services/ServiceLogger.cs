﻿using System;
using Test.Architecture.Domain.Interface;

namespace Test.Architecture.Common.Services
{
    public class ServiceLogger : ILogger
    {
        /// <summary>
        /// Important injected into the Common for Service Call
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        bool ILogger.Log(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Called the microService for distributed logging message:" + message);
            Console.ForegroundColor = ConsoleColor.Green;

            return true;
        }
    }
}