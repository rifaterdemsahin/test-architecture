﻿using System;
using Autofac;
using Test.Architecture.Domain.Model;

namespace Test.Architecture.DataAccess
{
    public class SqlRepository : IRepository
    {
        //Criticcal part inservices
        private readonly IComponentContext container;

        public SqlRepository(IComponentContext container)
        {
            this.container = container;
        }

        //Concrete Implementation
        public bool Save(BalanceOperation myOp)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Saved To PostgreSQL!!!");
            Console.ForegroundColor = ConsoleColor.Green;
            return true;
        }

        //Generic Implementation
        public T Get<T>(int id ) where T : class
        {
            Console.WriteLine("SELECT * FROM User Where ID=" + id);
            var result = container.Resolve<User>();
            result.ID = 123;
            result.LastName = "Hikmet";
            result.Name = "Nazım";
            result.Balance = 2000;
            return result as T;
        }


        public T GetByEmail<T>(string email) where T : class
        {
            Console.WriteLine("SELECT * FROM User Where Email=" + email);
            var result = container.Resolve<User>();
            result.ID = 1342343;
            result.LastName = "Kazım";
            result.Name = "Karabekir";
            result.Balance = 2;
            return result as T;
        }
    }
}
