﻿using Test.Architecture.Domain.Model;

namespace Test.Architecture.DataAccess
{
    public class UserRepository
    {
        private readonly IRepository myRepository;

        public UserRepository(IRepository myRepository)
        {
            this.myRepository = myRepository;
        }

        public User GetByID(int id)
        {
            var result = myRepository.Get<User>(id);
            return result;
        }
    }
}
