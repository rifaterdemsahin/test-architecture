﻿namespace Test.Architecture.Domain.Interface
{
    /// <summary>
    /// Service ve Repository interfaces makes the domain richer for non Anemic domains
    /// 
    /// </summary>
    public interface ILogger
    {
        new bool Log(string message);
    }
}