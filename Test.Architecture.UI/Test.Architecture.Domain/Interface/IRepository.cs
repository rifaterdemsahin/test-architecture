﻿using Test.Architecture.Domain.Model;

namespace Test.Architecture.DataAccess
{
    /// <summary>
    /// Service ve Repository interfaces makes the domain richer for non Anemic domains
    /// 
    /// </summary>
    public interface IRepository
    {
        //Concrete version
        bool Save(BalanceOperation myOp);

        //Generic version
        T Get<T>(int id) where T : class;
        T GetByEmail<T>(string email) where T : class;

        
    }
}