﻿using System;
using Test.Architecture.DataAccess;

namespace Test.Architecture.Domain.Model
{
    public class BalanceOperation
    {
        //IoC >>>>>>>>>>>>> comes into play when domain needs to get RICH instead of ANEMIC DOMAIN MODEL
        private readonly IRepository _balanceRepository;

        public BalanceOperation(IRepository balanceRepository)
        {
            _balanceRepository = balanceRepository;
        }

        //IoC >>>>>>>>>>> filled staff

        /// <summary>
        /// Balance operation,User da constructor olduğu için burası üzerinden 
        /// bağlanmamaktadır.
        /// </summary>
        public User MainUser { get; set;}
        public User RelatedUser { get; set; }

        public DateTime CreateDate { get; set; }

        public int Amount { get; set; }
        public string Status { get; set; }

        public bool Validate()
        {
            if (Amount < 100)
            {
                Console.WriteLine("Balance Operation validated by domain Model. It is lower than 100");
                return true;
            }
            return false;
        }

        public bool Save()
        {
            var result = false;
            result = _balanceRepository.Save(this);
            return result;
        }
    }
}
