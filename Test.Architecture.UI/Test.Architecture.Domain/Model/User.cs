﻿using System;
using Autofac;
using Test.Architecture.DataAccess;

namespace Test.Architecture.Domain.Model
{
    public class User
    {
        private readonly IComponentContext _container;

     

        #region Classical Anemic Domain
        public int ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Balance { get; set; }
        #endregion

        #region Richer By Relations

        private readonly BalanceOperation _myOperation;

        public User(BalanceOperation myOperation, IComponentContext container)
        {
            _myOperation = myOperation;
            myOperation.MainUser = this;
            _container = container;
        }
        #endregion

        #region Richer By Using Service
        public bool SaveWithdrawalMoney(int amount)
        {
            var result = false;
            _myOperation.Amount = amount;
            if (_myOperation.Validate())
            {
                result = _myOperation.Save();
                Balance -= amount;
            }
               
            return result;
        }

        public BalanceOperation CancelWithdrawalMoney(int amount)
        {
            _myOperation.Amount = amount;
            if (_myOperation.Validate())
            {
                _myOperation.Status = "Canceled";
                _myOperation.Save();
                Balance += amount;
            }
            return _myOperation;
        }
     
        #endregion

        #region Richer By Business rules/Calculations/Validations

        #endregion

  

        public BalanceOperation SendMoney(int amount, string receiverEmail)
        {
            var relatedUser = _container.Resolve<IRepository>().GetByEmail<User>(receiverEmail);
            if (_myOperation.Validate())
            {
                _myOperation.Status = "Approved";
                _myOperation.Amount = -amount;
                _myOperation.RelatedUser = relatedUser;
                _myOperation.Save();
                Balance -= amount;
            }
            relatedUser.GetMoney(amount, this);
    
            return _myOperation;
        }

        public void GetMoney(int amount, User relatedUser)
        {
            if (_myOperation.Validate())
            {
                _myOperation.Status = "Approved";
                _myOperation.Amount = amount;
                _myOperation.RelatedUser = relatedUser;
                _myOperation.Save();
                Balance += amount;
            }
        }

        
    }
}
