﻿using System;
namespace Test.Architecture.MicroService.Logging.Helper
{
    interface ILoggingService
    {
        bool log(string message);
    }
}
