﻿using System.Web.Mvc;

namespace Test.Architecture.MicroService.Logging
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
