﻿using System.Web.Mvc;

namespace Test.Architecture.MicroService.Logging.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
