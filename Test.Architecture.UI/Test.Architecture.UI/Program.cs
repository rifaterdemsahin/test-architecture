﻿using System;
using Autofac;
using Microsoft.Practices.ServiceLocation;
using Test.Architecture.BusinessLayer;
using Test.Architecture.Common;
using Test.Architecture.Common.Aop;
using Test.Architecture.Common.Services;
using Test.Architecture.DataAccess;
using Test.Architecture.Domain.Interface;
using Test.Architecture.Domain.Model;
using System.Diagnostics.Contracts;

namespace Test.Architecture.UI
{
    //http://www.infoq.com/articles/ddd-in-practice
    class Program
    {
        //Critical Driver/ROOT Container. For Loose Coupling
        public static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            RegisterIoC();

            Console.WriteLine("Program starts");
            ErrorRunner();
            for (int i = 0; i < 3; i++)
                FastUserActions();

            UserTransaction();
            Console.WriteLine("---------------------------------");
            UserTransactionFailing();
            Console.WriteLine("---------------------------------");
            UserTransactionFailingOverUser();
            Console.WriteLine("---------------------------------");
            UserTransactionCancelWithdrawalMoneyOverUser();
            Console.WriteLine("Program ends");
            Console.Read();

        }


        private static void RegisterIoC()
        {
            Console.WriteLine("IoC BootStrap Processing");
            var builder = new ContainerBuilder();

            Console.WriteLine("IoC Register for Service objects decoupling from the domain");

            builder.RegisterType<ServiceLogger>().As<ILogger>().InstancePerDependency().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType<MemoryLogger>().As<ILogger>().InstancePerDependency().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.RegisterType<SqlRepository>().As<IRepository>().InstancePerDependency();
            builder.RegisterType<UserRepository>().As<UserRepository>().InstancePerDependency();

            Console.WriteLine("IoC Register for Domain objects decoupling from themselves");
            builder.RegisterType<BalanceOperation>().As<BalanceOperation>().InstancePerDependency();
            builder.RegisterType<BalanceRequestOperation>().As<BalanceRequestOperation>().InstancePerDependency();

            //User property olarak kullandığı yerlerde Circular olarak dönmesini engellemektedir.
            //Constructor ve Property farklı konfigure edilirse çalışır.
            //Constructor ve Constructor konfigure edilirse hata verir.
            builder.RegisterType<User>().As<User>().InstancePerDependency().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            //builder.Register(GlobalServiceLocator.InstanceMemoryLogger).ExternallyOwned();
            //builder.Register(GlobalServiceLocator.InstanceServiceLogger).ExternallyOwned();


            builder.RegisterType(typeof(MemoryLogger));


            Container = builder.Build();

            var locator = new Autofac.Extras.CommonServiceLocator.AutofacServiceLocator(Container);
            ServiceLocator.SetLocatorProvider(() => locator);


            Console.WriteLine("IoC Container is Build!");

        }

        //   [ExceptionAspect(LoggerType = LogType.Memory)]
        [ExceptionAspect]
        private static void ErrorRunner()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Added Common reference first than postssharp reference to UI for exception");
            Console.ForegroundColor = ConsoleColor.Green;

            int a = 3;
            int b = 3;
            Console.WriteLine(b / (a - 3));//zero divide exception

        }

        [ExceptionAspect()]
        private static void FastUserActions()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Fast in memory Operations");
            Console.ForegroundColor = ConsoleColor.Green;
            Guid? myGuid = new Guid();
            myGuid = null;
            Console.WriteLine(myGuid.Value.ToByteArray());
        }

        [ExceptionAspect()]
        private static void UserTransaction()
        {
            Console.WriteLine("Direct Business Layer Call:UserTransaction started with 15 btc withdraw");
            Console.WriteLine("BalanceRequestOperation object got from Container!");
            var myOp = Container.Resolve<BalanceRequestOperation>();

            var status = myOp.SaveWithdrawalMoney(15);
            Console.WriteLine("TransactionStatus:" + status);
            Console.WriteLine("UserTransaction ended");
        }

        [ExceptionAspect()]
        private static void UserTransactionFailing()
        {
            Console.WriteLine("Direct Business Layer Call:UserTransaction failing started with 999 btc withdraw");
            var myOp = Container.Resolve<BalanceRequestOperation>();
            var status = myOp.SaveWithdrawalMoney(9999);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("TransactionStatus:" + status);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("UserTransaction failing ended");
            //No User directly calling the service layer....Service layer gets big.Domain layer goes to anemic.
        }


        [ExceptionAspect()]
        private static void UserTransactionFailingOverUser()
        {
            Console.WriteLine("UserTransactionFailingOverUser failing started with 777 btc withdraw");
            //Critical NO >>>>   var myUser = new UserRepository().GetByID(23);
            //Critical NO >>>> myUser.Operation.SaveWithdrawalMoney(9999);

            var myUser = Container.Resolve<UserRepository>().GetByID(23);
            var status = myUser.SaveWithdrawalMoney(777);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("TransactionStatus:" + status);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("UserTransactionFailingOverUser failing ended");
            //Methods in Domain objects , Domain gets rich service gets thin....just repeating business rules goes to Service Layer
        }

        [ExceptionAspect()]
        private static void UserTransactionCancelWithdrawalMoneyOverUser()
        {
            Console.WriteLine("UserTransactionCancelWithdrawalMoneyOverUser failing started with 3 btc withdraw");

            var mySenderUser = Container.Resolve<UserRepository>().GetByID(23);
            var myOperation = mySenderUser.CancelWithdrawalMoney(3);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("myOperation Amount:" + myOperation.Amount);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("UserTransactionCancelWithdrawalMoneyOverUser ended");
            //Methods in Domain objects , Domain gets rich service gets thin....just repeating business rules goes to Service Layer
        }


        [ExceptionAspect()]
        private static void UserTransactionToOthersUser()
        {
            Console.WriteLine("UserTransactionToOthersUser 3 btc");

            var mySenderUser = Container.Resolve<UserRepository>().GetByID(23);

            var myOperation = mySenderUser.SendMoney(3, "alici@gmail.com");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("myOperation Amount:" + myOperation.Amount);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("UserTransactionToOthersUser ended");
            //Methods in Domain objects , Domain gets rich service gets thin....just repeating business rules goes to Service Layer
            divide(0, 0);
        }


        [ExceptionAspect()]
        private static int divide(int a ,int b)
        {
            Contract.Requires(b != 0);
            return a / b;
        }
    }


}
