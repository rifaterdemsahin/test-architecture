﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Test.Architecture.BusinessLayer;
using Test.Architecture.DataAccess;
using Test.Architecture.Domain.Interface;
using Test.Architecture.Domain.Model;

namespace Test.Architecture.UnitTest
{
    [TestClass]
    public class UserDomainTest
    {
        private const int MainUserBalance = 30;
        private const int RelatedUserBalance = 20;
        private const int Amount = 3;
        private const string RelatedUserEmail = "related@with.me";
        private static IContainer _container;
        private static User _myMainUser;
        private static User _myRelatedUser;

          [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            var myRepository = new Mock<IRepository>();
        
            var builder = new ContainerBuilder();
            builder.RegisterInstance(new Mock<ILogger>().Object).As<ILogger>();
            builder.RegisterInstance(myRepository.Object).As<IRepository>();
            builder.RegisterType<BalanceOperation>().As<BalanceOperation>().InstancePerDependency();
            builder.RegisterType<BalanceRequestOperation>().As<BalanceRequestOperation>().InstancePerDependency();
            builder.RegisterType<User>().As<User>().InstancePerDependency().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
         
            _container = builder.Build();

            _myMainUser = _container.Resolve<User>();
     
            _myRelatedUser = _container.Resolve<User>();
            
            myRepository.Setup(x => x.GetByEmail<User>(RelatedUserEmail)).Returns(_myRelatedUser);
          
    }


        [TestInitialize]
        public void TestInit() {
            _myMainUser.Balance = MainUserBalance;
            _myRelatedUser.Balance = RelatedUserBalance;
        }
      
    
        /// <summary>
        /// 
        /// </summary>
        /// 
        
        [TestMethod]
        public void TestCancelWithdrawalMoney()
        {
            const int expected = MainUserBalance + Amount;
            var myOperation = _myMainUser.CancelWithdrawalMoney(Amount);
         
            //Problem1 : new in service objects....dont need mongo db in testing
            //Problem2 : new in domain objects.... cant mock and verify states of objects.
            Assert.AreEqual(_myMainUser.Balance, expected);
        }

        [TestMethod]
        public void TestSaveWithdrawalMoney()
        {
            const int expected = MainUserBalance - Amount;

            var myOperation = _myMainUser.SaveWithdrawalMoney(Amount);

            Assert.AreEqual(_myMainUser.Balance, expected);
        }

        [TestMethod]
        public void TestSendMoney()
        {
            const int expectedBalanceForMainUser = MainUserBalance - Amount;
            const int expectedBalanceForRelatedUser = RelatedUserBalance + Amount;
            
            var myOperation = _myMainUser.SendMoney(Amount, RelatedUserEmail);

            Assert.AreEqual(_myMainUser.Balance, expectedBalanceForMainUser);
            Assert.AreEqual(_myRelatedUser.Balance, expectedBalanceForRelatedUser);
        }
    }
}
